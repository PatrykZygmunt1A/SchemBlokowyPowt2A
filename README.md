# kotlinv-2/SchematyBlokoweP

## Wykonaj schematy blokowe i napisz program zgodnie z instrukcja podaną na lekcji.
Uwaga na lekcji pracowałeś w pliku **SchemBlokowyPowt**
Pobierz repozystorium **SchemBlokowyPowt2A**
Przekopiuj swoją pracę z lekcji:
* a) zawartość pliku Main.kt
* b) zawartość pliku README.md
* c) Wykonaj: branch, commit, push pull. 
* d) Umieść link do draw.io 
![Screenshot from 2024-02-21 22-02-34.png](Screenshot%20from%202024-02-21%2022-02-34.png)
![Screenshot from 2024-02-21 22-02-54.png](Screenshot%20from%202024-02-21%2022-02-54.png)
![Screenshot from 2024-02-21 22-03-10.png](Screenshot%20from%202024-02-21%2022-03-10.png)
![Screenshot from 2024-02-21 22-03-50.png](Screenshot%20from%202024-02-21%2022-03-50.png)
![Screenshot from 2024-02-21 22-04-29.png](Screenshot%20from%202024-02-21%2022-04-29.png)
### Pobierz repozytorium a następnie wykonaj 
#### Link do schematów blokowych wraz z przykładami kodu java, python 
#### https://www.tutorialkart.com/python/python-if-else/#gsc.tab=0
![Diagrambeztytułu.jpg](Diagrambeztytu%C5%82u.jpg)
![schemat-blokowy.jpg](schemat-blokowy.jpg)
![Diagrambeztytułu.jpg](Diagrambeztytu%B3u.jpg)
HTTPS clone URL: https://git.jetbrains.space/bartduraq/kotlinv-2/SchematyBlokoweP.git